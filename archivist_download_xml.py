#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to download .xml file
"""

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from urllib.parse import urlsplit, urlunsplit

from mylib import get_driver, url_base, archivist_login_all, get_base_url

import pandas as pd
import requests
import time
import sys
import os


n = 5

def download_link(url_location, output_file):
    """
    Given a URL, using requests library to download it to output_file
    """
    print("Download: " + url_location)
    url_get = requests.get(url_location)
    url_get.raise_for_status()
    with open(output_file, "wb") as f:
        f.write(url_get.content)


def download_xml(driver, uname, pw, df, out_file, output_xml_dir):
    """
    Loop over xml_name dictionary, check if exits a 'Download Export' button, download xml file
    """
    df_base_url = get_base_url(df)
    export_name = pd.Series(df_base_url.base_url.values, index=df.Instrument).to_dict()

    print("Got {} xml names".format(len(export_name)))
    # print(df)
    # print(export_name)

    # log in to all instance
    ok = archivist_login_all(driver, export_name.values(), uname, pw)
    print(ok)

    with open(os.path.join(os.path.dirname(output_xml_dir), out_file), "a") as f:
        f.write( ",".join(["prefix", "xml_date", "xml_location"]) + "\n")

    def log_to_csv(prefix, xml_date, xml_location):
        """append a line to spreadsheet with three values"""
        with open(os.path.join(os.path.dirname(output_xml_dir), out_file), "a") as f:
            f.write( ",".join([prefix, xml_date, xml_location]) + "\n")

    k = 0
    pre_url = None
    for prefix, url in export_name.items():
        if url:
            print("Prefix number {}".format(k))

            if url != pre_url:
                print("Load 'Instrument Exports' page : " + url)
                driver.get(url)
                delay = 20*n # seconds
                try:
                    # find the input box
                    inputElement = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//input[@placeholder='Search by prefix (press return to perform search)']")))
                    print("Page is ready!")
                except TimeoutException:
                    print("Loading export page took too much time!")
            else:
                inputElement = pre_input
                # use backspace to clear text field
                inputElement.send_keys(Keys.CONTROL, 'a')
                inputElement.send_keys(Keys.BACKSPACE)

            print('Search prefix "{}"'.format(prefix))
            prefix = prefix.strip()
            inputElement.send_keys(prefix)

            # choose from dropdown to display all results on one page
            select = Select(driver.find_element(By.XPATH, "//select[@aria-label='rows per page']"))

            # select by visible text
            select.select_by_visible_text('All')

            # locate id and link
            #TODO: make this better
            trs = driver.find_elements(by=By.XPATH, value="html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")

            print("This page has {} rows, searching for matching row".format(len(trs)))
            matching_idx = []
            for i in range(0, len(trs)):
                tr = trs[i]
                if prefix == tr.find_elements(By.XPATH, "td")[1].text:
                    matching_idx.append(i)
            if len(matching_idx) == 0:
                log_to_csv(prefix, "n/a", "Could not find a row matching the prefix")
                continue
            elif len(matching_idx) > 1:
                log_to_csv(prefix, "n/a", "There was more than one row matching this prefix: will download first")
                # note, keep going...?

            tr = trs[matching_idx[0]]

            # get XML
            # column 1 is "ID"
            tr_id = tr.find_elements(by=By.XPATH, value="td")[0].text

            # column 2 is "Prefix"
            tr_prefix = tr.find_elements(by=By.XPATH, value="td")[1].text
            print(tr_prefix, tr_id)

            # column 4 is "Actions"
            actions = tr.find_elements(By.XPATH, "td")[3].text
            # "Download export " text
            if len([item for item in actions.split('\n\n') if item.startswith('DOWNLOAD EXPORT')]) > 0:
                download_text = [item for item in actions.split('\n\n') if item.startswith('DOWNLOAD EXPORT')][0]
                xml_date = download_text.split('\n')[-1]

                # need to have download button
                exportButton = tr.find_elements(By.LINK_TEXT, download_text)
                xml_location = exportButton[0].get_attribute("href")
                # print(xml_location)
            else:
                xml_location = None
                xml_date = ''

            if xml_location is None:
                log_to_csv(prefix, xml_date, "Skipping because no 'dowwnload export' button")
                continue

            print("  Downloading xml for " + prefix)
            split_url = urlsplit(url)
            output_file_dir = os.path.join(output_xml_dir, split_url.netloc.split('.')[0])
            if not os.path.exists(output_file_dir):
                os.makedirs(output_file_dir)

            out_xml_f = os.path.join(output_file_dir, prefix + ".xml")

            xml_get = requests.get(xml_location)
            xml_get.raise_for_status()
            with open(out_xml_f, "wb") as f:
                f.write(xml_get.content)

            log_to_csv(prefix, xml_date, xml_location)

            pre_url = url
            pre_input = inputElement
        k = k + 1


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]
    # prefixes
    df = pd.read_csv('Prefixes_to_export.txt', sep='\t')

    driver = get_driver()

    output_xml_dir = "export_xml"
    if not os.path.exists(output_xml_dir):
        os.makedirs(output_xml_dir)

    # download xml files
    out_file = os.path.join(output_xml_dir, "xml_list.csv")
    download_xml(driver, uname, pw, df, out_file, output_xml_dir)

    driver.quit()


if __name__ == "__main__":
    main()

