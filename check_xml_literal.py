#!/bin/env python3

"""
    Python 3
    Check downloaded xml file: qustion item / statement item contain literal text?
"""

import xml.etree.ElementTree as ET
import os


# xml namespace
ns = {'r': 'ddi:reusable:3_2',
      'i': 'ddi:instance:3_2',
      's': 'ddi:studyunit:3_2',
      'd': 'ddi:datacollection:3_2',
      'l': 'ddi:logicalproduct:3_2',
      'g': 'ddi:group:3_2'}


def parse_StatementItem(root):
    """
    input: root of ET xml
    output: StatementItem list of dict
    """
    StatementItem_list = []
    StatementItem_all = root.findall('./g:ResourcePackage/d:ControlConstructScheme/d:StatementItem', ns)

    for StatementItem in StatementItem_all:
        StatementItem_dict = {}
        StatementItem_dict['URN'] = StatementItem.find('r:URN', ns).text
        StatementItem_dict['Name'] = StatementItem.find('d:ConstructName/r:String', ns).text
        if StatementItem.find('d:DisplayText/d:LiteralText/d:Text', ns) is not None:
            StatementItem_dict['Literal'] = StatementItem.find('d:DisplayText/d:LiteralText/d:Text', ns).text
        else:
            StatementItem_dict['Literal'] = None

        StatementItem_list.append(StatementItem_dict)

    return StatementItem_list


def check_statement_literal(root):
    """
    Check if statement has empty literal.
    input: root of ET xml
    output: statement name which has no literal
    """
    prob_statement = []
    statement_list = parse_StatementItem(root)
    for statement in statement_list:
        if statement['Literal'] is None:
            prob_statement.append(statement['Name'])

    return prob_statement


def parse_QuestionItem(root):
    """
    input: root of ET xml
    output: QuestionItem list of dict
    """
    QuestionItem_list = []
    QuestionItem_all = root.findall('./g:ResourcePackage/d:QuestionScheme/d:QuestionItem', ns)

    for QuestionItem in QuestionItem_all:
        QuestionItem_dict = {}
        QuestionItem_dict['URN'] = QuestionItem.find('r:URN', ns).text
        QuestionItem_dict['Name'] = QuestionItem.find('d:QuestionItemName/r:String', ns).text
        if QuestionItem.find('d:QuestionText/d:LiteralText/d:Text', ns) is not None:
            QuestionItem_dict['Literal'] = QuestionItem.find('d:QuestionText/d:LiteralText/d:Text', ns).text
        else:
            QuestionItem_dict['Literal'] = None

        QuestionItem_list.append(QuestionItem_dict)

    return QuestionItem_list


def check_question_literal(root):
    """
    Check if question has empty literal.
    input: root of ET xml
    output: question name which has no literal
    """
    prob_question = []
    question_list = parse_QuestionItem(root)
    for question in question_list:
        if question['Literal'] is None:
            prob_question.append(question['Name'])

    return prob_question


def main():
    main_dir = 'export_xml'

    out_dir = 'check_xml_literal'
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    input_names = []
    for path, subdirs, files in os.walk(main_dir):
        for name in files:
            if name.endswith('.xml'):
                input_names.append(os.path.join(path, name))

    for input_name in input_names:
        xmlFile = input_name
        tree = ET.parse(xmlFile, parser=ET.XMLParser(encoding="utf-8"))
        root = tree.getroot()

        # which statement has no literal text?
        prob_s_name = check_statement_literal(root)

        # which question item has no literal text?
        prob_qi_name = check_question_literal(root)

        out_file_name = os.path.join(out_dir, 'check_' + os.path.basename(input_name))
        print(input_name)
        print(out_file_name)
        print(out_dir)
        with open(out_file_name, 'w') as out_file, open(os.path.join(out_dir, 'no_missing.txt'), 'a') as nomissing_file:
            if prob_s_name == [] and prob_qi_name == []:
                nomissing_file.write(input_name + '\t' + 'No missing literals for both question item and statement.\n')
            elif prob_s_name != [] and prob_qi_name == []:
                out_file.write('No missing literals for question item.\n')
                out_file.write('The following statements has empty literal:\n')
                out_file.write(', '.join(str(x) for x in prob_s_name))
            elif prob_s_name == [] and prob_qi_name != []:
                out_file.write('No missing literals for statement.\n')
                out_file.write('The following question item has empty literal:\n')
                out_file.write(', '.join(str(x) for x in prob_qi_name))
            elif prob_s_name != [] and prob_qi_name != []:
                out_file.write('The following question item has empty literal:\n')
                out_file.write(', '.join(str(x) for x in prob_qi_name))
                out_file.write('The following statements has empty literal:\n')
                out_file.write(', '.join(str(x) for x in prob_s_name))

        if os.path.getsize(out_file_name) == 0:
            os.remove(out_file_name)


if __name__ == "__main__":
    main()

